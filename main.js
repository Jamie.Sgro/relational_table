//Declare public variables
var screen = {width: 600, height: 400};
var block = [];

function preload() {

};

function setup() {
  createCanvas(screen.width, screen.height);
  stroke(200);
  for (var i = 0; i < 4; i++) {
    block[i] = new Block(20,i*40+20,20,20, id=i, adj=[]);
  };
  for (var i = 4; i < 8; i++) {
    block[i] = new Block(screen.width-40,i*40-140,20,20, id=i, adj=[]);
  };

  block[0].adj = [4,5];
  block[1].adj = [7];
  block[2].adj = [4,7];
  block[3].adj = [5,6,7];
  block[4].adj = [0,1];
  block[5].adj = [0,3];
  block[6].adj = [3];
  block[7].adj = [1,2,3];

};

function draw() {
  background(100);
  for (var i in block) {
    block[i].draw();
  };

  textSize(32)
  fill(255,255,255)
  text("Click on a box to toggle", screen.width/4, screen.height/2)
};

function mousePressed() {
  for (var i in block) {
    if (block[i].checkClick(mouseX, mouseY)) {
      block[i].selected();
    };
  };
};

function mouseReleased() {

};

function keyPressed() {
  if (key == "A") {

  };
};

function keyReleased() {
  if (key == "A") {

  };
};
