class Block {
  constructor(x,y,w,h, id, adj) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.id = id;
    this.adj = adj;

    this.clicked = false;

    this.fill = [255,255,255];
  };

  draw() {
    fill(this.fill);
    rect(this.x, this.y, this.w, this.h);

    if (this.clicked) this.drawConnections();
  };


  /**
   * checkClick - returns whether the mouse if over the current element
   *
   * @param  {int} x mouseX position
   * @param  {int} y mouseY position
   * @return {bool}   description
   */
  checkClick(x, y) {
    var cond1 = x > this.x,
      cond2 = x < this.x + this.w,
      cond3 = y > this.y,
      cond4 = y < this.y + this.h;

    return cond1 && cond2 && cond3 && cond4;
  };

  selected() {
    this.fill = this.switchCol();
    this.clicked = this.flick(this.clicked);
  };

  switchCol() {
    if (this.arraysMatch(this.fill, [255,255,255])) return [0,0,0];
    return [255,255,255];
  };

  arraysMatch(arr1, arr2) {
    if (arr1.length !== arr2.length) return false;

    for (var i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) return false;
    };
    return true;
  };

  flick(bool) {
    if (bool) return false;
    return true;
  }

  drawConnections() {
    for (var i in this.adj) {
      var id = this.adj[i];
      line(this.x, this.y, block[id].x, block[id].y)
    };
  };
};
